// Завдання
// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
//     Технічні вимоги:
//
//     У файлі index.html лежить розмітка двох полів вводу пароля.
//     Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
//     Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
//
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.


const labels = document.querySelectorAll('.input-wrapper');
const btn = document.querySelector('.btn');
btn.disabled = true;

let inpPass;
let checkInpPass;

const form = document.querySelector('.password-form');
const error = document.querySelector('.error');


labels.forEach(el => {

    const showPassword = el.querySelector('.icon-password');
    const passwordInput = el.querySelector('[type=password]');

    passwordInput.addEventListener('input', e => {

        if (e.target.value !== '') {
            btn.disabled = false
        }

        if (passwordInput.dataset.inp === 'pass') {
            inpPass = e.target.value;
        } else {
            if (passwordInput.dataset.inp === 'check') {
                checkInpPass = e.target.value;
            }
        }
    })

    showPassword.addEventListener('click', e => {

        if (passwordInput.getAttribute('type') === 'password') {
            passwordInput.setAttribute('type', 'text');
        } else {
            passwordInput.setAttribute('type', 'password')
        }
    })
})

form.addEventListener('submit', e => {

    e.preventDefault();

    if (inpPass === checkInpPass) {
        alert('You are welcome');
    } else {
        error.innerText = 'Потрібно ввести однакові значення';
    }

})
